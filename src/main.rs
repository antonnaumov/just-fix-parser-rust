extern crate chrono;
extern crate file_scanner;
extern crate regex;
#[macro_use]
extern crate structopt;

mod entry;
mod message;
mod builder;
mod processor;
mod parser;

use std::error::Error;
use std::fs::File;
use std::path::PathBuf;
use processor::PrintMessageProcessor;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "fix-parser", about = "Parse Market Data Full Refresh messages from the input file")]
struct Opt {
    #[structopt(name = "FILE", parse(from_os_str))]
    file: PathBuf
}

fn main() {
    let opt = Opt::from_args();
    match File::open(opt.file) {
        Err(reason) => panic!("Could not open the file {}", reason.description()),
        Ok(mut file) => {
            let mut processor = PrintMessageProcessor::new();
            processor.print_header();
            parser::parse(&mut file, &mut processor);
            processor.print_footer();
        }
    }
}