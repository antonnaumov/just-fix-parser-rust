use builder::MessageBuilder;
use entry::Entry;
use processor::MessageProcessor;
use std::io::Read;


fn read_next<R: Read>(reader: &mut R) -> Option<u8> {
    let mut buff = [0];
    match reader.read(&mut buff).map(|n| {
        if n > 0 {
            Some(buff[0])
        } else {
            None
        }
    }) {
        Ok(r) => r,
        Err(err) => panic!(err)
    }
}

pub fn parse<R: Read, P: MessageProcessor>(reader: &mut R, processor: &mut P) {
    let mut builder = MessageBuilder::new();
    let mut entry = Entry::new();
    let mut cursor: Vec<u8> = Vec::new();
    let mut next = read_next(reader);
    let mut recording = false;
    while next.is_some() {
        let b = next.unwrap();
        match b {
            56 => {
                entry.record_check_sum(b);
                cursor.push(b);
                recording = true;
            }
            1 => {
                if recording {
                    entry.record_check_sum(b);
                    entry.record_value(cursor.clone());
                    builder.with_entry(processor, entry);
                    entry = Entry::new();
                    cursor.clear();
                }
            }
            61 => {
                if recording {
                    entry.record_check_sum(b);
                    entry.record_key(cursor.clone());
                    cursor.clear();
                }
            }
            _ => {
                if recording {
                    entry.record_check_sum(b);
                    cursor.push(b);
                }
            }
        }
        next = read_next(reader);
    }
}

#[cfg(test)]
mod tests {
    use chrono::*;
    use processor::AccumulateMessageProcessor;
    use message::OperationType;
    use super::*;

    #[test]
    fn single_message_parsing() {
        let data = "8=FIX.4.49=14235=W34=049=justtech52=20180206-21:43:36.00056=user262=TEST55=EURUSD268=2269=0270=1.31678271=100000.0269=1270=1.31667271=100000.010=057";
        let mut bytes = data.as_bytes();
        let mut processor = AccumulateMessageProcessor::new();
        parse(&mut bytes, &mut processor);
        assert_eq!(processor.rejected_message_count, 0);
        assert_eq!(processor.accepted_messages.len(), 1);

        let msg = processor.accepted_messages.pop().unwrap();
        assert_eq!(msg.symbol, "EURUSD");
        assert_eq!(msg.timestamp, NaiveDateTime::from_timestamp(Utc.ymd(2018, 2, 6).and_hms(21, 43, 36).timestamp(), 0));
        assert_eq!(msg.operations.len(), 2);

        match msg.operations.iter().find(|opt| opt.op_type == OperationType::Buy) {
            Some(buy) => {
                assert_eq!(buy.price, 1.31678);
                assert_eq!(buy.amount, 100000.0);
            }
            None => assert_eq!(false, true)
        };
        match msg.operations.iter().find(|opt| opt.op_type == OperationType::Sell) {
            Some(sell) => {
                assert_eq!(sell.price, 1.31667);
                assert_eq!(sell.amount, 100000.0);
            }
            None => assert_eq!(false, true)
        };
    }

    #[test]
    fn reject_message_with_invalid_check_sum() {
        let data = "8=FIX.4.49=14235=W34=049=justtech52=20180206-21:43:36.00056=user262=TEST55=EURUSD268=2269=0270=1.31678271=100000.0269=1270=1.31667271=100000.010=055";
        let mut bytes = data.as_bytes();
        let mut processor = AccumulateMessageProcessor::new();
        parse(&mut bytes, &mut processor);
        assert_eq!(processor.rejected_message_count, 1);
        assert_eq!(processor.accepted_messages.len(), 0);
    }

    #[test]
    fn reject_message_with_not_enough_operations() {
        let data = "8=FIX.4.49=14235=W34=049=justtech52=20180206-21:43:36.00056=user262=TEST55=EURUSD268=2269=0270=1.31678271=100000.010=055";
        let mut bytes = data.as_bytes();
        let mut processor = AccumulateMessageProcessor::new();
        parse(&mut bytes, &mut processor);
        assert_eq!(processor.rejected_message_count, 1);
        assert_eq!(processor.accepted_messages.len(), 0);
    }

    #[test]
    fn reject_message_does_not_represent_buy_sell_operations() {
        let data = "8=FIX.4.19=6135=A34=152=20000426-12:05:0698=0108=3010=157";
        let mut bytes = data.as_bytes();
        let mut processor = AccumulateMessageProcessor::new();
        parse(&mut bytes, &mut processor);
        assert_eq!(processor.rejected_message_count, 1);
        assert_eq!(processor.accepted_messages.len(), 0);
    }

    #[test]
    fn parse_messages_from_file() {
        use std::fs::File;
        use std::error::Error;

        let mut file = match File::open("example-fix-data.bin") {
            Err(reason) => panic!("File [example-fix-data.bin] opening filed because [{}]", reason.description()),
            Ok(file) => file
        };
        let mut processor = AccumulateMessageProcessor::new();
        parse(&mut file, &mut processor);
        assert_eq!(processor.rejected_message_count, 0);
        assert_eq!(processor.accepted_messages.len(), 10);
    }
}