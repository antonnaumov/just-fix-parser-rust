use chrono::NaiveDateTime;

#[derive(Debug, Clone, PartialEq)]
pub enum OperationType {
    Buy,
    Sell
}

impl OperationType {
    pub fn new(op_type: String) -> OperationType {
        if op_type == "0".as_ref() {
            OperationType::Buy
        } else {
            OperationType::Sell
        }
    }
}

#[derive(Debug, Clone)]
pub struct Operation {
    pub op_type: OperationType,
    pub price: f32,
    pub amount: f32
}

impl Operation {
    pub fn new(op_type: OperationType, price: f32, amount: f32) -> Operation {
        Operation {
            op_type,
            price,
            amount
        }
    }
}

#[derive(Debug, Clone)]
pub struct Message {
    pub symbol: String,
    pub timestamp: NaiveDateTime,
    pub operations: Vec<Operation>
}

