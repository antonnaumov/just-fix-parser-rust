use message::{Message, OperationType};
use std::result::Result;

#[derive(Debug)]
pub struct MessageBuildError(MessageBuildErrorKind);

pub const INCONSISTENT_FIELD: MessageBuildError = MessageBuildError(MessageBuildErrorKind::InconsistentField);
pub const INVALID_TIMESTAMP: MessageBuildError = MessageBuildError(MessageBuildErrorKind::InvalidTimestampFormat);
pub const INVALID_NUMBER: MessageBuildError = MessageBuildError(MessageBuildErrorKind::InvalidNumberFormat);
pub const INVALID_CHECK_SUM: MessageBuildError = MessageBuildError(MessageBuildErrorKind::InvalidCheckSum);
pub const INVALID_OPERATION_COUNT: MessageBuildError = MessageBuildError(MessageBuildErrorKind::InvalidOperationsCount);

#[derive(Debug)]
enum MessageBuildErrorKind {
    InconsistentField,
    InvalidTimestampFormat,
    InvalidNumberFormat,
    InvalidCheckSum,
    InvalidOperationsCount
}

pub type BuildResult<T> = Result<T, MessageBuildError>;

#[allow(dead_code)]
pub struct AccumulateMessageProcessor {
    pub accepted_messages: Vec<Message>,
    pub rejected_message_count: u32
}

#[allow(dead_code)]
impl AccumulateMessageProcessor {
    pub fn new() -> AccumulateMessageProcessor {
        AccumulateMessageProcessor {
            accepted_messages: Vec::new(),
            rejected_message_count: 0
        }
    }
}

pub struct PrintMessageProcessor {
    pub accepted_message_count: u32,
    pub rejected_message_count: u32
}

impl PrintMessageProcessor {
    pub fn new() -> PrintMessageProcessor {
        PrintMessageProcessor {
            accepted_message_count: 0,
            rejected_message_count: 0
        }
    }

    pub fn print_header(&self) {
        self.print_splitter();
        println!("| Symbol | Buy Price | Sell Price | Buy Amount | Sell Amount |        Timestamp        |");
        self.print_splitter();
    }

    pub fn print_footer(&self) {
        self.print_splitter();
        println!(
            "| Total: | Rejected: {rejected:>11}, Accepted: {accepted:>12}                               |",
            accepted = self.accepted_message_count,
            rejected = self.rejected_message_count
        );
        self.print_splitter();
    }

    fn print_splitter(&self) {
        println!("+--------+-----------+------------+------------+-------------+-------------------------+")
    }

    fn print_message(&self, message: Message) {
        let (buy_price, buy_amount) = self.find_operation_price_amount(message.clone(), OperationType::Buy);
        let (sell_price, sell_amount) = self.find_operation_price_amount(message.clone(), OperationType::Sell);
        println!(
            "| {:^6} |  {:^.5}  |  {:^.5}   |  {:^.2} |  {:^.2}  | {:^23} |",
            message.symbol,
            buy_price,
            sell_price,
            buy_amount,
            sell_amount,
            message.timestamp.to_string()
        )
    }

    fn find_operation_price_amount(&self, message: Message, op_type: OperationType) -> (f32, f32) {
        match message.operations.iter().find(|op|op.op_type == op_type) {
            Some(op) => (op.price, op.amount),
            None => (0.0, 0.0)
        }
    }
}

pub trait MessageProcessor {
    fn process(&mut self, result: BuildResult<Message>) {
        match result {
            Ok(message) => MessageProcessor::accept_message(self, message),
            Err(err) => MessageProcessor::reject_message(self,err)
        }
    }

    fn accept_message(&mut self, message: Message);

    fn reject_message(&mut self, error: MessageBuildError);
}

impl MessageProcessor for AccumulateMessageProcessor {
    fn accept_message(&mut self, message: Message) {
        self.accepted_messages.push(message);
    }

    fn reject_message(&mut self, _error: MessageBuildError) {
        self.rejected_message_count += 1;
    }
}

impl MessageProcessor for PrintMessageProcessor {
    fn accept_message(&mut self, message: Message) {
        self.print_message(message);
        self.accepted_message_count += 1;
    }

    fn reject_message(&mut self, _error: MessageBuildError) {
        self.rejected_message_count += 1;
    }
}