use entry::Entry;
use processor::*;
use chrono::NaiveDateTime;
use message::{Message, Operation, OperationType};
use std::str::FromStr;

#[derive(Debug, Clone)]
struct OperationBuilder {
    op_type: String,
    price: String,
    amount: String
}

#[derive(Debug, Clone)]
pub struct MessageBuilder {
    symbol: Option<String>,
    timestamp: Option<String>,
    op_count: Option<String>,
    op_type: Option<String>,
    op_price: Option<String>,
    op_amount: Option<String>,
    operations: Vec<OperationBuilder>,
    check_sum: u32
}

impl MessageBuilder {
    pub fn new() -> MessageBuilder {
        MessageBuilder {
            symbol: None,
            timestamp: None,
            op_count: None,
            op_type: None,
            op_price: None,
            op_amount: None,
            operations: Vec::new(),
            check_sum: 0
        }
    }

    pub fn with_entry<P: MessageProcessor>(&mut self, processor: &mut P, entry: Entry) -> &mut MessageBuilder {
        match (entry.key, entry.value) {
            (Some(8), _) => {
                self.clean();
                self.check_sum += entry.check_sum;
            },
            (Some(10), value) => {
                processor.process(self.build(value))
            },
            (Some(52), value) => {
                self.timestamp = value;
                self.check_sum += entry.check_sum;
            },
            (Some(55), value) => {
                self.symbol = value;
                self.check_sum += entry.check_sum;
            },
            (Some(268), value) => {
                self.op_count = value;
                self.check_sum += entry.check_sum;
            },
            (Some(269), value) => {
                self.op_type = value;
                self.check_sum += entry.check_sum;
                self.compose_op();
            },
            (Some(270), value) => {
                self.op_price = value;
                self.check_sum += entry.check_sum;
                self.compose_op();
            },
            (Some(271), value) => {
                self.op_amount = value;
                self.check_sum += entry.check_sum;
                self.compose_op();
            },
            _ => {
                self.check_sum += entry.check_sum;
            }

        };
        self
    }

    fn build(&self, check_sum: Option<String>) -> BuildResult<Message> {
        let check_sum_val_res = self.validate_check_sum(check_sum);
        let op_count_val_res = self.validate_operations_count();
        let sym_res = match self.symbol.to_owned() {
            Some(v) => Ok(v),
            None => Err(INCONSISTENT_FIELD)
        };
        let ts_res = match self.timestamp.to_owned() {
            Some(v) => {
                NaiveDateTime::parse_from_str(&v, "%Y%m%d-%T%.3f")
                    .map_err(|_err| INVALID_TIMESTAMP)
            },
            None => Err(INCONSISTENT_FIELD)
        };
        let ops_res = self.build_operations();
        if check_sum_val_res.is_ok() && op_count_val_res.is_ok() {
            sym_res.and_then(|s| {
                ts_res.and_then(|ts| {
                    ops_res.and_then(|ops| {
                        Ok(Message {
                            symbol: s,
                            timestamp: ts,
                            operations: ops
                        })
                    })
                })
            })
        } else if check_sum_val_res.is_err() {
            Err(check_sum_val_res.err().unwrap())
        } else {
            Err(op_count_val_res.err().unwrap())
        }
    }

    fn compose_op(&mut self) {
        if self.op_type.is_some() && self.op_price.is_some() && self.op_amount.is_some() {
            self.operations.push(OperationBuilder {
                op_type: self.op_type.clone().unwrap(),
                price: self.op_price.clone().unwrap(),
                amount: self.op_amount.clone().unwrap()
            });
            self.clean_op();
        }
    }

    fn clean_op(&mut self) {
        self.op_type = None;
        self.op_price = None;
        self.op_amount = None;
    }

    fn clean(&mut self) {
        self.symbol =None;
        self.timestamp = None;
        self.op_count = None;
        self.check_sum = 0;
        self.operations.clear();
        self.clean_op();
    }

    fn validate_check_sum(&self, check_sum: Option<String>) -> BuildResult<()> {
        match check_sum {
            Some(str) => {
                match u32::from_str(&str) {
                    Ok(sum) => {
                        if sum != (self.check_sum % 256) {
                            Err(INVALID_CHECK_SUM)
                        } else {
                            Ok(())
                        }
                    },
                    Err(_err) => Err(INVALID_NUMBER)
                }
            },
            None => Err(INVALID_NUMBER)
        }
    }

    fn validate_operations_count(&self) -> BuildResult<()> {
        match self.op_count.to_owned() {
            Some(count) => {
                match usize::from_str(&count) {
                    Ok(op_count) =>
                        if op_count != self.operations.len() {
                            Err(INVALID_OPERATION_COUNT)
                        } else {
                            Ok(())
                        }
                    Err(_err) => Err(INVALID_NUMBER)
                }
            },
            None =>
                Err(INCONSISTENT_FIELD)
        }
    }

    fn build_operations(&self) -> BuildResult<Vec<Operation>> {
        let mut r: Vec<Operation> = Vec::new();
        let mut err: Option<MessageBuildError> = None;
        for ob in self.operations.to_owned() {
            let p = ob.price.parse::<f32>();
            let a = ob.amount.parse::<f32>();
            if p.is_ok() && a.is_ok() {
                r.push(Operation::new(OperationType::new(ob.op_type), p.ok().unwrap(), a.ok().unwrap()))
            } else {
                err = Some(INCONSISTENT_FIELD)
            }
        };
        match err {
            Some(err) => Err(err),
            None => Ok(r)
        }
    }
}
