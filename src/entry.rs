use std::str::FromStr;

#[derive(Debug, Clone)]
pub struct Entry {
    pub key: Option<u32>,
    pub value: Option<String>,
    pub check_sum: u32
}

impl Entry {
    pub fn new() -> Entry {
        Entry {
            key: None,
            value: None,
            check_sum: 0
        }
    }

    pub fn record_key(&mut self, source: Vec<u8>) -> &mut Entry {
        let _ = String::from_utf8(source).map_err(|_str_parse_err| None::<u32>).map(|str| u32::from_str(&str))
            .and_then(|cast_res| cast_res.map_err(|_cast_err| None::<u32>).map(|key| Some(key)))
            .map(|key_opt|self.key = key_opt);
        self
    }

    pub fn record_value(&mut self, source: Vec<u8>) -> &mut Entry {
        let _ = String::from_utf8(source)
            .map_err(|_str_parse_err|None::<String>)
            .map(|str| Some(str))
            .map(|opt_val| self.value = opt_val);
        self
    }

    pub fn record_check_sum(&mut self, b: u8) -> &mut Entry {
        self.check_sum += b as u32;
        self
    }
}
